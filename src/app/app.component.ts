import {Component, OnInit} from '@angular/core';
import {Log} from '../decorators/log-method';
import {LifecycleLogger} from '../decorators/log-lifecycle';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

@LifecycleLogger('ngOnInit', 'ngOnChanges')
export class AppComponent implements OnInit {
  title = 'app';

  constructor() {
  }

  ngOnInit() {
    console.log('do the right work. do the work right');
  }

  @Log()
  react(event) {
    console.log('do the right work. event: ', event.type);
  }
}
