import {environment} from '../environments/environment';

export function Log(): MethodDecorator {
  return (target: Object, key: string | symbol, descriptor: PropertyDescriptor) => {
    if (!environment.production) {
      const originalMethod = descriptor.value;
      descriptor.value = function (...args: any[]) {
        console.log(`%c ${target.constructor.name}#${key}\n`, `color: #4CAF50; font-weight: bold`, ...args);
        originalMethod.apply(target, args);
        console.log(`%c ${target.constructor.name}#${key}\n`, `color: #AF4c50; font-weight: bold`);
      };
    }
  };
}
