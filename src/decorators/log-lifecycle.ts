import {environment} from '../environments/environment';

export function LifecycleLogger(...hooks: string[]): ClassDecorator {
  return function (constructor: any) {
    if (!environment.production) {
      /*
      const HOOKS = [

        'ngOnInit',
        'ngOnChanges',
        'ngDoCheck',
        'ngAfterContentInit',
        'ngAfterContentChecked',
        'ngAfterViewInit',
        'ngAfterViewChecked',
        'ngOnDestroy'
      ];
      */
      const HOOKS = new Array<string>();
      hooks.forEach(h => {
        HOOKS.push(h);
      });

      const component = constructor.name;

      HOOKS.forEach(hook => {
        const original = constructor.prototype[hook];

        constructor.prototype[hook] = function (...args) {
          console.log(`%c ${component}#${hook}`, `color: #4CAF50;`, ...args);
          original && original.apply(this, args);
        };
      });
    }
  };
}
